<!-- Aleksi Vähätaini Tik19km -->


<?php require_once 'inc/top.php'; ?>

    <!-- kuvien lukumäärän näyttäminen -->
    <div class="container" id="images"></div>

   <div class='container'>
    <div class="gallery">
 
    <?php 
    // Image extensions
    $image_extensions = array("png","jpg","jpeg","gif");

    // Target directory
    // Määritellään kansio mistä kuvat löytyvät
    $dir = 'uploads/';
    if (is_dir($dir)){
        
        // avataan kyseinen kansio
      if ($dh = opendir($dir)){
        $count = 1;

        // Read files
        // luetaan tiedostot avatusta kansiosta niin kauan kuin niitä riittää
        while (($file = readdir($dh)) !== false){

          if($file != '' && $file != '.' && $file != '..'){
 
            // Thumbnail image path 
            //etsitään pikkukuvan sijainti
            $thumbnail_path = "uploads/thumbs/".$file;

            // Image path
            // etsitään kuvan sijainti
            $image_path = "uploads/".$file;
 
            $thumbnail_ext = pathinfo($thumbnail_path, PATHINFO_EXTENSION);
            $image_ext = pathinfo($image_path, PATHINFO_EXTENSION);

            // Check its not folder and it is image file
            // tarkistetaan että on kuvatiedosto eikä esim kansio
            if(!is_dir($image_path) && 
                in_array($thumbnail_ext,$image_extensions) && 
                in_array($image_ext,$image_extensions)){
       ?>
                
                <!-- Image -->
                <!-- Tässä tehdään kuvalle elementti millä se näytetään etusivulla -->
                <a class="" href="<?= $image_path; ?> ">
                  <img src="<?= $thumbnail_path; ?>">
                </a>
                
       <?php
                // Break // tässä päätetään montako pikkukuvaa näytetään rinnakkain
                if( $count%4 == 0){
       ?>
                   <div class="clear"></div>
       <?php 
                }
                $count++;
             }
          }
 
         }
           closedir($dh);
        }
      }
      ?>
      </div>
    </div>

<?php require_once 'inc/bottom.php'; ?>